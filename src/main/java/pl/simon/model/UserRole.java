package pl.simon.model;

public enum UserRole {

	ADMIN,
	USER;
}
