package pl.simon.model;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import lombok.ToString;

@ToString
public class SampleEntity {

	private String name;
	private String description;
	private ZonedDateTime time;

	public SampleEntity(String name, String description, ZonedDateTime zonedDateTime) {
		super();
		this.name = name;
		this.description = description;
		this.time = zonedDateTime;
	}

	public SampleEntity(){
		
	}
	

	public ZonedDateTime getTime() {
		return time;
	}

	public void setTime(ZonedDateTime time) {
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
