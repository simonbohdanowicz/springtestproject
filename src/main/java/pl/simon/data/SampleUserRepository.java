package pl.simon.data;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import pl.simon.model.User;
import pl.simon.model.UserRole;

@Service
public class SampleUserRepository {

	private final List<User> users = Lists.newArrayList(
			User.builder().username("batman").password("robin").role(UserRole.ADMIN).build(),
			User.builder().username("spiderman").password("hobgoblin").role(UserRole.USER).build());
	
	public List<User> getUsers(){
		return users;
	}
	
	public Optional<User> getUserByUsername(String username){
		return users.stream().filter(u -> u.getUsername().equals(username)).findFirst();
	}
}
