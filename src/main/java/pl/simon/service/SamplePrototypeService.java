package pl.simon.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype")
public class SamplePrototypeService {

	public String introduceYourself(){
		return "I am always new: "+this.hashCode();
	}
	
}
