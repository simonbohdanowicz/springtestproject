package pl.simon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import pl.simon.websocket.SampleWebSocketHandler;

@Component
public class SampleSingletonService {

	@Autowired
	SampleWebSocketHandler sampleWebSocketHandler;

	public String introduceYourselfAfterFiveSeconds() throws InterruptedException{
		Thread.sleep(5000);
		return introduceYourself();
	}
	
	public String introduceYourself(){
		return "I am always the same: "+this.hashCode();
	}

	@Async
	public void executeAsyncAction(){
		try{
			Thread.sleep(10000);
			sampleWebSocketHandler.sendMessageToSocket("Can't rememeber who am I", "I was sent after ten seconds!!!");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
