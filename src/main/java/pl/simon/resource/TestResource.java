package pl.simon.resource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.simon.model.SampleEntity;

@RestController
public class TestResource {

	@RequestMapping(method=RequestMethod.GET, path="/", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity index() {
		return new SampleEntity("Sample name", (String) null, ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/sth-wrong", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity responseWithError(){
		throw new TestRestControllerAdvice.IDontLikeItException("this exception is on purpouse");
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/params/{pathParam}", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity pathParam(@PathVariable(name="pathParam") String pathParam) {
		return new SampleEntity("Request with pathParam.", "This is path param:"+pathParam, ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/params", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity requestAttribute(@RequestParam(name = "date")  @DateTimeFormat(pattern="MMddyyyy") LocalDate requestParam) {
		return new SampleEntity("Request with requestParam.", "This request has date param :"+requestParam, ZonedDateTime.now());
	}

	@RequestMapping(method=RequestMethod.GET, path="/header", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity requestAttribute(@RequestHeader(name = "metaInfo")  String context) {
		return new SampleEntity("Request with header metaInfo.", "This request has header metaInfo with content:"+context, ZonedDateTime.now());
	}

}
