package pl.simon.resource;

import java.time.ZonedDateTime;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.simon.model.SampleEntity;
import pl.simon.service.SamplePrototypeService;
import pl.simon.service.SampleSingletonService;

@RestController
@RequestMapping(path = "/admin/")
public class TestAdminResource {

	private static final Logger logger = LoggerFactory.getLogger(TestAdminResource.class);
	@Autowired
	SampleSingletonService singletonService;
	
	@Autowired
	ApplicationContext applicationContext;
	
	@RequestMapping(method=RequestMethod.GET, path="/hello", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity index() {
		return new SampleEntity("Hello admin", "This is hello admin endpoint", ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/singleton", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity singletonService() {
		return new SampleEntity("Singleton service is saying.", singletonService.introduceYourself(), ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/prototype", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity prototypeService() {
		SamplePrototypeService prototypeService = applicationContext.getBean(SamplePrototypeService.class);
		return new SampleEntity("Prototype service is saying.", prototypeService.introduceYourself(), ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/async", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void asyncAction(@RequestParam(name = "info", required = false) String infoMessage) {
		singletonService.executeAsyncAction();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/callable", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	//https://www.javacodegeeks.com/2015/07/understanding-callable-and-spring-deferredresult.html
	public Callable<SampleEntity> callableEndpoit() {
		Callable<SampleEntity> callable = new Callable<SampleEntity>() {
			
			@Override
			public SampleEntity call() throws Exception {
				return new SampleEntity(singletonService.introduceYourselfAfterFiveSeconds(), null, null);
			}
		};
		
		try {
			callable.call();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("thread released");

		return callable;
	}
	
}
