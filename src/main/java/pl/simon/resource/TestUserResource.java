package pl.simon.resource;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.simon.model.SampleEntity;
import pl.simon.service.SamplePrototypeService;
import pl.simon.service.SampleSingletonService;
import pl.simon.websocket.SampleWebSocketHandler;

@RestController
@RequestMapping(path = "/user/")
public class TestUserResource {

	@Autowired
	SampleWebSocketHandler sampleWebSocketHandler;
	
	@Autowired
	SampleSingletonService singletonService;
	
	@Autowired
	SamplePrototypeService prototypeService;
	
	@RequestMapping(method=RequestMethod.GET, path="/hello", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity index() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return new SampleEntity("Hello user: "+authentication.getName(), "This is hello user endpoint.", ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/message", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public void sendSocketMessage(@RequestBody SampleEntity message) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		sampleWebSocketHandler.sendMessageToSocket(authentication.getName(), message.getName());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/singleton", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity singletonService() {
		return new SampleEntity("Singleton service is saying.", singletonService.introduceYourself(), ZonedDateTime.now());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/prototype", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public SampleEntity prototypeService() {
		return new SampleEntity("Prototype service is saying.", prototypeService.introduceYourself(), ZonedDateTime.now());
	}
	
}
