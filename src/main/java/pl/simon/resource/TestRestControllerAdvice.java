package pl.simon.resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class TestRestControllerAdvice {

	@ExceptionHandler({ IDontLikeItException.class })
    public ResponseEntity<Object> handleAccessDeniedException(IDontLikeItException ex, WebRequest request) {
        return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.I_AM_A_TEAPOT);
    }
	
	static class IDontLikeItException extends RuntimeException{
		
		IDontLikeItException(String message){
			super(message);
		}
	}
	
}
