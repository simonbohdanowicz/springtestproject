package pl.simon.websocket;

import java.util.List;
import java.util.Objects;

import org.jooq.lambda.Unchecked;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.google.common.collect.Lists;

@Component
public class SampleWebSocketHandler extends TextWebSocketHandler {

    final List<WebSocketSession> sessions = Lists.newArrayList();

    public void sendMessageToSocket(String author, String message) {
       sessions.stream().filter(s -> Objects.nonNull(s) && s.isOpen()).forEach(Unchecked.consumer(s -> s.sendMessage(new TextMessage(author + " says \""+ message +"\""))));
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        System.out.println("Connection established");
        sessions.add(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message)
            throws Exception {
        if ("CLOSE".equalsIgnoreCase(message.getPayload())) {
            session.close();
            sessions.remove(session);
        } else {
            System.out.println("Received:" + message.getPayload());
        }
    }
}
