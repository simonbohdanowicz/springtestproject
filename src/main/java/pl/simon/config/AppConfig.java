package pl.simon.config;

import java.time.ZonedDateTime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class AppConfig {
	
	@Bean
	public ZonedDateTime currentTime(){
		return ZonedDateTime.now();
	}
	
}
