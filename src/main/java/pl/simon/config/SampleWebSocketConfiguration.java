package pl.simon.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import pl.simon.websocket.SampleWebSocketHandler;

@Configuration
@EnableWebSocket
public class SampleWebSocketConfiguration implements WebSocketConfigurer {

	@Autowired
	SampleWebSocketHandler sampleWebSocketHandler;
	
	@Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(sampleWebSocketHandler, "/socket").setAllowedOrigins("*");
    }
	
}
